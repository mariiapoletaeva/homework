package io.test.automation.homeworklesson5.steps

import io.fortest.automation.enums.ElementType
import io.fortest.automation.element.Element
import io.qameta.allure.Step
import io.test.automation.homeworklesson5.pages.CartPage

import static io.fortest.automation.extension.PageWaiter.*


class OrderSteps {

    @Step("Заполняем форму заказа")
    private static void fillOrderForm(Map<String, Element> customerInfoMap){

        def customer = customerInfoMap
        customer.each {
            value, field ->
            if(field.type == ElementType.SELECT){
                CartPage.getOrderForm().elementClick(field.name, value)
            } else {
                CartPage.getOrderForm().fillField(field.name, value)
            }
        }
    }

    static void fillForm(Map<String, Element> customerInfoMap){
        fillOrderForm(customerInfoMap)
        CartPage.getOrderForm().elementClick()
    }

    @Step("Подтверждаем заказ")
    static void confirmOrder(){
        waitJQuery()
        CartPage.getConfirmOrderButton().scrollTo()
        CartPage.getConfirmOrderButton().click()
    }
}
