package io.test.automation.homeworklesson5.steps

import com.codeborne.selenide.*
import io.fortest.automation.extension.Extension
import io.qameta.allure.Attachment
import io.qameta.allure.Step
import io.test.automation.homeworklesson5.pages.CartPage
import io.test.automation.homeworklesson5.pages.ItemPage
import io.test.automation.homeworklesson5.pages.MainPage

import static io.fortest.automation.actions.SimpleWaitAction.*


class SelectSteps {

    @Step("Заходим на главную страницу магазина уточек {0}")
    static void openMainPage(String uri) {
        Selenide.open(uri)
    }

    static void selectProduct(){
        previewProduct()
        Boolean visible = ItemPage.getDuckSizeSelector().isDisplayed()
        if(visible){
            setProductCount()
        }
        addProductToCart()
    }

    @Step("Получаем стоммость товаров в корзине")
    @Attachment
    static String getCartAmount(){
        def amount = MainPage.goToCartLink
        return Extension.convertPrice(amount.text)
    }

    @Step("Переходим на страницу оформления заказа")
    static void goToConfirmOrder(){
        MainPage.getGoToCartLink().click()
    }

    @Step("Получаем итоговую сумму заказа")
    @Attachment
    static String getOrderAmount(){
        def amount = CartPage.getPaymentDue()
        waitElement(amount)
        return amount.text
    }

    @Step("Получаем случайный номер уточки")
    @Attachment
    private static int selectProductIndex(ElementsCollection products){
        return Extension.generateRandomItem(products.size())
    }

    @Step("Выбираем и переходим к просмотру товара")
    private static void previewProduct(){
        ElementsCollection availableProducts = MainPage.getProducts()
        int item = selectProductIndex(availableProducts)
        SelenideElement product =  availableProducts.get(item)
        product.click()
    }

    @Step("Выбираем случаейный размер уточки")
    private static void setProductCount(){
        def duckSizes = ItemPage.getProductSizes()
        duckSizes.get(Extension.generateRandomItem(duckSizes.size())).click()
    }

    @Step("Отправляем уточку в корзину")
    private static void addProductToCart(){
        def addButton = ItemPage.getAddToCartButton()
        waitElement(addButton)
        addButton.click()

        checkCartCount(1)
    }

    @Step("Проверяем, что в корзине {0} шт")
    private static void checkCartCount(int count){
        def cartCount = MainPage.qetQuantity()
        waitText(cartCount, count.toString())
    }
}

