package io.test.automation.homeworklesson5.steps

import static io.fortest.automation.actions.SimpleWaitAction.waitText
import static io.test.automation.homeworklesson5.pages.ResultOrderPage.getFinalTitle


class CheckSuccessSteps {

    static void seeResultMessage(String message){
        waitText(getFinalTitle(), message)
    }
}
