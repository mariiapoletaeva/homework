package io.test.automation.homeworklesson5.pages

import io.qameta.allure.Attachment
import io.qameta.allure.Step

import static com.codeborne.selenide.Selenide.*
import com.codeborne.selenide.*


class MainPage {

    @Step("Получаем список доступных для покупки уточек")
    @Attachment
    static ElementsCollection getProducts() {
        return $$("li.product")
    }

    @Step("Получаем кнопку для перехода в корзину")
    @Attachment
    static SelenideElement getGoToCartLink() {
        return $("#cart > a.content > span.formatted_value")
    }

    @Step("Получаем карточку с количеством элементов в корзине")
    @Attachment
    static SelenideElement qetQuantity() {
        return $("#cart > a.content > span.quantity")
    }
}
