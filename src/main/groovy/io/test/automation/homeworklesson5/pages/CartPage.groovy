package io.test.automation.homeworklesson5.pages

import com.codeborne.selenide.SelenideElement
import io.qameta.allure.Attachment
import io.qameta.allure.Step
import io.test.automation.homeworklesson5.containers.OrderForm

import static com.codeborne.selenide.Selenide.$


class CartPage {

    @Step("Получаем сумму заказа на форме")
    @Attachment
    static SelenideElement getPaymentDue() {
       return $(" tr.footer > td:nth-child(2) > strong")
    }

    @Step("Получаем форму для заказа")
    @Attachment
    static OrderForm getOrderForm(){
        def orderForm = new OrderForm()
        orderForm.self = $("div.billing-address")
        return orderForm
    }

    @Step("Получаем кнопку для подтверждения заказа")
    @Attachment
    static SelenideElement getConfirmOrderButton(){
        return $("div.confirm > p > button")
    }
}
