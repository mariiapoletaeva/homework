package io.test.automation.homeworklesson5.pages

import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.SelenideElement
import io.qameta.allure.Attachment
import io.qameta.allure.Step

import static com.codeborne.selenide.Selenide.*


class ItemPage {

    @Step("Получаем кнопку для добавления уточки в корзину")
    @Attachment
    static SelenideElement getAddToCartButton() {
        return $x("//button[@name='add_cart_product']")
    }

    @Step("Получаем список возможных размеров уточки")
    @Attachment
    static ElementsCollection getProductSizes(){
        return $$x("//option[not(contains(.,'-- Select --'))]")
    }

    @Step("Получаем выбиральщик размера уточки")
    @Attachment
    static SelenideElement getDuckSizeSelector(){
        return $("[name='options[Size]']")
    }
}
