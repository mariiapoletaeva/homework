package io.test.automation.homeworklesson5.pages

import com.codeborne.selenide.SelenideElement
import io.qameta.allure.Attachment
import io.qameta.allure.Step

import static com.codeborne.selenide.Selenide.$


class ResultOrderPage {

    @Step("Получаем плашку с сообщением о статусе заказа")
    @Attachment
    static SelenideElement getFinalTitle() {
        return $(".title")
    }
}
