package io.test.automation.homeworklesson5.containers

import com.codeborne.selenide.ElementsContainer
import com.codeborne.selenide.SelenideElement
import io.qameta.allure.Attachment
import io.qameta.allure.Step


class OrderForm extends ElementsContainer{

    @Step("Получаем поле для ввода {0}")
    @Attachment
    private SelenideElement getField(String name) {
        return self.$x("//input[@name='$name']")
    }

    @Step("Получаем значение {1} из списка {0}")
    @Attachment
    private  SelenideElement getDropdownItem(String name, String value){
        return self.$x("//select[@name='$name']/option[@value='$value']")
    }

    @Step("Получаем кнопку для сохранения адреса")
    @Attachment
    private SelenideElement getSaveButton() {
        return self.$x("//button[@name='set_addresses']")
    }

    void fillField(String fieldName, String fieldValue) {
        getField(fieldName).value = fieldValue
    }

    void elementClick(String selectName, String selectValue){
        getDropdownItem(selectName, selectValue).click()
    }

    void elementClick(){
        getSaveButton().click()
    }
}
