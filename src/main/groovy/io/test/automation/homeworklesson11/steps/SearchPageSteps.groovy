package io.test.automation.homeworklesson11.steps

import com.codeborne.selenide.Selenide
import io.qameta.allure.Step
import io.test.automation.homeworklesson11.pages.SearchPage
import io.fortest.automation.actions.ElementAction
import io.fortest.automation.actions.SimpleWaitAction

class SearchPageSteps {

    @Step("Открываем страницу поиска")
    static void openMainPage(String uri) {
        Selenide.open(uri)
    }

    @Step("Закрываем брaузер")
    static void stopBrowser(){
        Selenide.close()
    }

    @Step("Вносим в поле значение для поиска и выполняем поиск")
    static List<String> searchByRequest(String request){
        SearchPage.getInputField().value = request
        SearchPage.getSearchButton().click()

        def elements = SearchPage.getSearchResult()
        SimpleWaitAction.waitElementsCount(elements, 1)
        return ElementAction.extractAttribute(elements)
    }
}
