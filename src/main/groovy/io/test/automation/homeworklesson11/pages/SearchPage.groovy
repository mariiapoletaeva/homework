package io.test.automation.homeworklesson11.pages

import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.SelenideElement
import io.qameta.allure.Attachment
import io.qameta.allure.Step

import static com.codeborne.selenide.Selenide.$
import static com.codeborne.selenide.Selenide.$$x

class SearchPage {

    @Step("Получаем кнопку поиска")
    @Attachment
    static SelenideElement getSearchButton() {
        return $("form[role='search']  button[role='button']")
    }

    @Step("Получаем поле для ввода")
    @Attachment
    static SelenideElement getInputField() {
        return $("input#text")
    }

    @Step("Получаем коллекцию результатов поиска")
    @Attachment
    static ElementsCollection getSearchResult() {
        return $$x("//div/h2/a[@href]")
    }
}
