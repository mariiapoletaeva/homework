package io.fortest.automation.extension

import com.codeborne.selenide.WebDriverRunner
import io.qameta.allure.Step
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait


class PageWaiter {

    @Step("Ожидаем отработки JQuery в течение 30 секунд")
    static waitJQuery() {

        new WebDriverWait(getDriver(), 30).until({
            try {
                executeJavaScript("return jQuery.active == 0")
            } catch (Throwable e) {
            }
        })
    }

    private static WebDriver getDriver() {
        if (WebDriverRunner.hasWebDriverStarted()) {
            return WebDriverRunner.webDriver
        } else {
            return null
        }
    }

    private static <T> T executeJavaScript(String jsCode, Object... arguments) {
        return (T) ((JavascriptExecutor) getDriver()).executeScript(jsCode, arguments)
    }
}
