package io.fortest.automation.extension

import io.qameta.allure.Attachment
import io.qameta.allure.Step


class Extension {

    @Step("Выбирает случайный номер от 1 до {0}")
    @Attachment
    static int generateRandomItem(int size){
        Random random = new Random()
        return random.nextInt(size - 1)
    }

    static String convertPrice(String price){
        if (price.contains(".")){
            return price
        } else {
            return price + ".00"
        }
    }
}
