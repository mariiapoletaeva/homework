package io.fortest.automation.element

import io.fortest.automation.enums.ElementType


class Element {
    String name
    ElementType type

    Element(String name, ElementType type){
        this.name = name
        this.type = type
    }
}
