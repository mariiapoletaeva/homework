package io.fortest.automation.actions

import com.codeborne.selenide.*
import io.qameta.allure.Step


class SimpleWaitAction {

    @Step("Ожидаем появления элемента {0}")
    static void waitElement(SelenideElement element){
        element.shouldBe(Condition.visible)
    }

    @Step("Ожидаем, когда элемент {0} будет содержать текст {1}")
    static void waitText(SelenideElement element, String text){
        element.shouldHave(Condition.exactText(text))
    }

    @Step("Ожидаем, когда количество элементов в коллекции будет больше {0}")
    static void waitElementsCount(ElementsCollection elements, int count){
        elements.shouldBe(CollectionCondition.sizeGreaterThan(1))
    }
}
