package io.fortest.automation.actions

import com.codeborne.selenide.ElementsCollection
import io.qameta.allure.Attachment
import io.qameta.allure.Step

class ElementAction {

    @Step("Получаем массив ссылок из коллекции элементов")
    @Attachment
    static List<String> extractAttribute(ElementsCollection collection){
        List<String> list = new ArrayList<>()
        collection.each {
            list.add(it.getAttribute("href"))
        }
        return list
    }
}
