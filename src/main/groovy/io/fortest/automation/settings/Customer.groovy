package io.fortest.automation.settings

import io.fortest.automation.element.Element
import io.fortest.automation.enums.ElementType
import io.qameta.allure.Attachment
import io.qameta.allure.Step


class Customer {

    @Step("Создаём покупателя")
    @Attachment
     static Map<String, Element> CustomerInfoMap(){
        HashMap<String, Element> hashMap = new HashMap<>()

        hashMap.put("123", new Element("tax_id", ElementType.INPUT))
        hashMap.put("CatInc", new Element("company", ElementType.INPUT))
        hashMap.put("Push", new Element("firstname", ElementType.INPUT))
        hashMap.put("Cat", new Element("lastname", ElementType.INPUT))
        hashMap.put("Cat street 14/6 11", new Element("address1", ElementType.INPUT))
        hashMap.put("94622", new Element("postcode", ElementType.INPUT))
        hashMap.put("San Francisco", new Element("city", ElementType.INPUT))
        hashMap.put("US", new Element("country_code", ElementType.SELECT))
        hashMap.put("CA", new Element("zone_code", ElementType.SELECT))
        hashMap.put("cat@cat.com", new Element("email", ElementType.INPUT))
        hashMap.put("777666", new Element("phone", ElementType.INPUT))

        return hashMap
    }
}
