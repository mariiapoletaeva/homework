package io.fortest.automation.settings

class SearchExample {
    public final static String gradleRequest = "Gradle"
    public final static String groovyRequest = "Groovy"
    public final static String testngRequest = "TestNG"
}
