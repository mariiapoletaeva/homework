package io.homework.myauto

import io.qameta.allure.Description
import io.qameta.allure.Epic
import io.qameta.allure.Feature
import io.qameta.allure.Issue
import io.qameta.allure.Link
import io.qameta.allure.Severity
import io.qameta.allure.SeverityLevel
import io.qameta.allure.Step
import io.qameta.allure.Story
import io.qameta.allure.TmsLink
import org.testng.annotations.Test
import org.openqa.selenium.By

import static com.codeborne.selenide.Selenide.*

@Epic("Duck Shop")
@Link("http://litecart.stqa.ru/en/")
class LessonThreeLitecart {

    @Test
    @Issue("TICKET-2")
    @Feature("Региональные настройки")
    @Description("Выбор и сохранение региональных настроек")
    @Severity(SeverityLevel.MINOR)
    @TmsLink("TC-2")
    @Story("Country = US, Region = FL")
    @Step("Выбираем и сохраняем региональные настройки")
    void changeRegion(){
        open("http://litecart.stqa.ru/en/")

        def changeButton = $("a.fancybox-region")
        changeButton.click()

        def country = $(By.xpath("//select[@name='country_code']/option[@value='US']"))
        country.click()

        def region = $(By.xpath("//select[@name='zone_code']/option[@value='FL']"))
        region.click()

        def saveButton = $(By.xpath("//button[@name='save']"))
        saveButton.click()
    }
    @Test
    @Issue("TICKET-3")
    @Feature("Распродажа уточек")
    @Description("Вывод списка уточек без скидки")
    @Severity(SeverityLevel.NORMAL)
    @TmsLink("TC-3")
    @Story("Выбираются уточки без скидки")
    @Step("Получаем и печатаем список уточек без скидки")
    void choiceGoodsWithoutSale(){
        open("http://litecart.stqa.ru/en/acme-corp-m-1/")

        def content = $$(By.xpath("//li[contains(@class, 'product')][not(contains(.,'Sale'))]"))
        content.each {
            def itemName = it.$("div.name")
            def itemPrice = it.$("div.price-wrapper")
            println("$itemName.text -- $itemPrice.text")
        }
    }
    @Test
    @Issue("TICKET-4")
    @Feature("Распродажа уточек")
    @Description("Вывод списка уточек со скидкой")
    @Severity(SeverityLevel.NORMAL)
    @TmsLink("TC-4")
    @Story("Выбираются уточки со скидкой")
    @Step("Получаем и печатаем список уточек со скидкой")
    void choiceGoodsWithSale(){
        open("http://litecart.stqa.ru/en/acme-corp-m-1/")

        def content = $$(By.xpath("//li[contains(@class, 'product')][(contains(.,'Sale'))]"))
        content.each {
            def itemName = it.$("div.name")
            def itemPrice = it.$("strong.campaign-price")
            println("$itemName.text -- $itemPrice.text")
        }
    }
}
