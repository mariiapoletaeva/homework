package io.homework.myauto

import io.fortest.automation.settings.Customer
import io.fortest.automation.settings.Message
import io.fortest.automation.settings.Uri

import static io.test.automation.homeworklesson5.steps.CheckSuccessSteps.*
import static io.test.automation.homeworklesson5.steps.OrderSteps.*
import static io.test.automation.homeworklesson5.steps.SelectSteps.*
//import static org.junit.Assert.assertEquals

import io.qameta.allure.Description
import io.qameta.allure.Epic
import io.qameta.allure.Feature
import io.qameta.allure.Issue
import io.qameta.allure.Link
import io.qameta.allure.Severity
import io.qameta.allure.SeverityLevel
import io.qameta.allure.Story
import io.qameta.allure.TmsLink
import org.testng.annotations.Test

@Epic("Duck Shop")
@Feature("Оформление заказа")
@Link("http://litecart.stqa.ru/en/")
@Issue("TICKET-1")
class LessonFiveLitecart {
    @Test
    @Description("Оформить заказ на одну уточку через корзину")
    @Severity(SeverityLevel.NORMAL)
    @TmsLink("TC-1")
    @Story("Заказ на случайную уточку случайного размера успешно оформлен")
    void choiceOneDuck(){
        openMainPage(Uri.mainPage)
        selectProduct()
        String cartAmount = getCartAmount()
        goToConfirmOrder()
        String orderAmount = getOrderAmount()
        //TODO переделать assert
        //assertEquals(cartAmount, orderAmount)

        fillForm(Customer.CustomerInfoMap())
        confirmOrder()
        seeResultMessage(Message.successMessage)
    }
}
