package io.homework.myauto

import groovy.util.logging.Slf4j

import io.qameta.allure.Step
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import io.fortest.automation.settings.SearchExample
import io.fortest.automation.settings.Uri
import static io.test.automation.homeworklesson11.steps.SearchPageSteps.openMainPage
import static io.test.automation.homeworklesson11.steps.SearchPageSteps.searchByRequest
import static io.test.automation.homeworklesson11.steps.SearchPageSteps.stopBrowser

@Slf4j
class Lesson11TestNG {

    @BeforeMethod(alwaysRun = true)
    void openSearchPage(){
        openMainPage(Uri.pageYaRu)
    }

    @AfterMethod(alwaysRun = true)
    void closeBrowser(){
        stopBrowser()
    }

    @Test(groups = ["LESSON_11", "CASE_1"])
    @Step("Case 1")
    void searchAndPrintGradle(){
        def linkList = searchByRequest(SearchExample.gradleRequest)
        for (String item : linkList){
            log.info(item)
        }
    }

    @Test(groups = ["LESSON_11", "CASE_2"])
    @Step("Case 2")
    void searchAndPrintGroovy(){
        def linkList = searchByRequest(SearchExample.groovyRequest)
        for (String item : linkList){
            log.info(item)
        }
    }

    @Test(groups = ["LESSON_11", "CASE_3"])
    @Step("Case 3")
    void searchAndPrintTestNG(){
        def linkList = searchByRequest(SearchExample.testngRequest)
        for (String item : linkList){
            log.info(item)
        }
    }
}
